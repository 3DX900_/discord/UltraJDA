package fr._3dx900.ultrajda;

import fr._3dx900.ultrajda.managers.EventManager;
import fr._3dx900.ultrajda.managers.UltraManager;

import net.dv8tion.jda.core.JDA;

public class UltraJDA extends UltraManager {

    private static JDA api;

    public UltraJDA(final JDA api) {
        super();
        UltraJDA.api = api;

        api.addEventListener(new EventManager());
    }

    public static JDA getAPI() { return UltraJDA.api; }
}
