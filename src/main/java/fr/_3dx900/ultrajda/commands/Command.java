package fr._3dx900.ultrajda.commands;

import fr._3dx900.ultrajda.UltraJDA;
import fr._3dx900.ultrajda.managers.CommandManager;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;

public class Command {

    private String command;
    private String[] description;
    private String[] usages;
    private String[] channelsId;
    private Permission[] permissions;
    private String[] guildId;
    private Boolean deleteCommandAfter;
    private Object instance;
    private Method method;

    public Command(CommandHandler handler, Object instance, Method method) {
        this.command = handler.command();
        this.description = handler.description();
        this.usages = handler.usages();
        this.channelsId = handler.channels();
        this.permissions = handler.permission();
        this.guildId = handler.guild();
        this.deleteCommandAfter = handler.deleteCommandAfter();
        this.instance = instance;
        this.method = method;

        UltraJDA.getCommandManager().addCommand(this.command, this);
    }

    public String getCommand() {
        return this.command;
    }

    public String[] getDescription() {
        return this.description;
    }

    public String[] getUsages() {
        return this.usages;
    }

    public String[] getChannels() {
        return this.channelsId;
    }

    public Permission[] getPermissions() {
        return permissions;
    }

    public String[] getGuilds() {
        return this.guildId;
    }

    public Boolean isCommandDeleteAfter() {
        return this.deleteCommandAfter;
    }

    public Object getInstance() {
        return instance;
    }

    public Method getMethod() {
        return this.method;
    }


    public void handle(Message message) throws InvocationTargetException, IllegalAccessException {
        if(!message.getMember().hasPermission(this.getPermissions())) {
            message.getChannel().sendMessage(new EmbedBuilder()
                    .setAuthor("Error !", null, "https://cdn3.iconfinder.com/data/icons/security-flat/614/3376_-_No_Entry_Sign-512.png")
                    .setDescription(message.getMember().getEffectiveName() + " you do not have the correct permissions to do that.")
                    .build()).complete();
            return;
        }
        Parameter[] parameters = this.getMethod().getParameters();
        Object[] objects = new Object[parameters.length];
        for(Integer i = 0; i < objects.length; i++) {
            if(parameters[i].getType() == Command.class) objects[i] = this;
            if(parameters[i].getType() == String.class) objects[i] = this.getCommand();
            if(parameters[i].getType() == String[].class) objects[i] = message.getContentDisplay().replace(UltraJDA.getCommandManager().getPrefix(message.getGuild().getIdLong()) + this.getCommand() + " ", "").split(" ");
            if(parameters[i].getType() == Guild.class) objects[i] = message.getGuild();
            if(parameters[i].getType() == JDA.class) objects[i] = message.getJDA();
            if(parameters[i].getType() == Message.class) objects[i] = message;
            if(parameters[i].getType() == MessageChannel.class) objects[i] = message.getChannel();
            if(parameters[i].getType() == TextChannel.class) objects[i] = message.getTextChannel();
            if(parameters[i].getType() == PrivateChannel.class) objects[i] = message.getPrivateChannel();
            if(parameters[i].getType() == Channel[].class) {
                Channel[] channels = new Channel[message.getMentionedChannels().size()];
                for(int i1 = 0; i1 < channels.length; i1++) {
                    channels[i1] = message.getMentionedChannels().get(i1);
                }
                objects[i] = channels;
            }
            if(parameters[i].getType() == Member.class) objects[i] = message.getMember();
            if(parameters[i].getType() == User.class) objects[i] = message.getMember().getUser();
            if(parameters[i].getType() == User[].class) objects[i] = message.getMentionedUsers();
            if(parameters[i].getType() == Role[].class) objects[i] = message.getMentionedRoles();
            if(parameters[i].getType() == Message.Attachment[].class) objects[i] = message.getAttachments();
            if(parameters[i].getType() == Emote[].class) objects[i] = message.getEmotes();
        }
        if(this.isCommandDeleteAfter())
            message.delete().complete();
        this.getMethod().invoke(this.getInstance(), objects);
    }
}
