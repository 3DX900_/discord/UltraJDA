package fr._3dx900.ultrajda.commands;

import net.dv8tion.jda.core.Permission;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface CommandHandler {

    String command();
    String[] description() default {"No description."};
    String[] usages() default {};
    Permission[] permission() default {};
    String[] channels() default {};
    String[] guild() default {};
    boolean deleteCommandAfter() default true;
}
