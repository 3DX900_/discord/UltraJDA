package fr._3dx900.ultrajda.event;

import fr._3dx900.ultrajda.UltraJDA;
import net.dv8tion.jda.core.events.ReadyEvent;

public class ReadyListener {

    public ReadyListener(final ReadyEvent event) {
        event.getJDA().getGuilds().forEach(guild -> UltraJDA.getCommandManager().setPrefix(guild, UltraJDA.getCommandManager().getDefaultPrefix()));
    }
}
