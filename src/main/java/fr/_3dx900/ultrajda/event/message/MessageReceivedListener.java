package fr._3dx900.ultrajda.event.message;

import fr._3dx900.ultrajda.UltraJDA;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

import java.lang.reflect.InvocationTargetException;

public class MessageReceivedListener {

    public MessageReceivedListener(final MessageReceivedEvent event) {
        if (event.getAuthor().isBot()) return;

        if (event.getMessage().getContentDisplay().startsWith(UltraJDA.getCommandManager().getPrefix(event.getGuild().getIdLong())) && UltraJDA.getCommandManager().isCommand(event.getMessage().getContentDisplay().split(" ")[0].replace(UltraJDA.getCommandManager().getPrefix(event.getGuild().getIdLong()), ""))) {
            final String command = event.getMessage().getContentDisplay().split(" ")[0].replace(UltraJDA.getCommandManager().getPrefix(event.getGuild().getIdLong()), "");
            try {
                UltraJDA.getCommandManager().getCommand(command).handle(event.getMessage());
            } catch (InvocationTargetException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }
}
