package fr._3dx900.ultrajda.managers;

import fr._3dx900.ultrajda.UltraJDA;
import fr._3dx900.ultrajda.commands.Command;
import fr._3dx900.ultrajda.commands.CommandHandler;
import net.dv8tion.jda.core.entities.Guild;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class CommandManager {

    private Map<String, Command> commands = new HashMap<>();

    private String defaultPrefix = "/";
    private Map<Guild, String> prefix = new HashMap<>();

    protected static void register(final Object commandClass) {
        for(Method method : commandClass.getClass().getDeclaredMethods()) {
            if(!method.isAnnotationPresent(CommandHandler.class)) continue;
            method.setAccessible(true);
            new Command(method.getAnnotation(CommandHandler.class), commandClass, method);
        }
    }

    public Map<String, Command> getCommands() {
        return commands;
    }

    public void addCommand(String command, Command instance) {
        this.commands.put(command, instance);
    }

    public String getDefaultPrefix() {
        return this.defaultPrefix;
    }

    public String getPrefix(final Long guildId) {
        return this.prefix.getOrDefault(UltraJDA.getAPI().getGuildById(guildId), this.getDefaultPrefix());
    }

    public void setDefaultPrefix(final String defaultPrefix) {
        this.defaultPrefix = defaultPrefix;
    }

    public void setPrefix(final String prefix) {
        for(Guild guild : this.prefix.keySet()) {
            this.prefix.put(guild, prefix);
        }
    }

    public void setPrefix(final Guild guild, final String prefix) {
        this.prefix.put(guild, prefix);
    }

    public Command getCommand(String commandName) {
        return this.commands.getOrDefault(commandName, null);
    }

    public Boolean isCommand(String commandName) {
        return this.commands.containsKey(commandName);
    }
}
