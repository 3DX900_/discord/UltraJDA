package fr._3dx900.ultrajda.managers;

import fr._3dx900.ultrajda.event.ReadyListener;
import fr._3dx900.ultrajda.event.message.MessageReceivedListener;
import net.dv8tion.jda.core.events.ReadyEvent;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class EventManager extends ListenerAdapter {

    @Override
    public void onReady(ReadyEvent event) {
        super.onReady(event);
        new ReadyListener(event);
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        super.onMessageReceived(event);
        new MessageReceivedListener(event);
    }
}
