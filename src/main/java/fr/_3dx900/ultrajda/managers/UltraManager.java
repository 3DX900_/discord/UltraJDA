package fr._3dx900.ultrajda.managers;

public abstract class UltraManager {

    private static CommandManager commandManager;

    public UltraManager() {
        UltraManager.commandManager = new CommandManager();
    }

    public static CommandManager getCommandManager() {
        return UltraManager.commandManager;
    }

    public static void registerCommand(final Object commandClass) {
        CommandManager.register(commandClass);
    }
}
